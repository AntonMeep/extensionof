with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Directories;

use Ada.Text_IO;
use Ada.Command_Line;

procedure ExtensionOf is
begin
	if Argument_Count = 0 then
		Put_Line("Usage: " & Command_Name & " <files>");
		return;
	end if;

	for I in Positive range 1..Argument_Count loop
		Put_Line(Ada.Directories.Extension(Argument(I)));
	end loop;
end ExtensionOf;
